import random

def print_rules():
    print("Bonjour ! Bienvenu au jeu des positions pour le violon.\nVoici les règles pour ce jeu:")
    print("\t- Il est demandé une note correspondant à une position et un doigt pour chaque corde du violon.")
    print("\t- Par défaut le nombre de question est 1")
    print("\t- Choissisez votre niveau de difficulté parmi celles qui vous sont proposées")
    print("\n\n\n")

def print_menu():
    print("==== Choix de difficulté ====")
    print("\t1. Débutant (seulement en première position)")
    print("\t2. Intermédiaire (positions 1,2,3,4)")
    print("\t3. Avancé (positions 1,2,3,4,5,6,7)")
    print("\t4. Personnalisée")
    print("\t5. Quitter")


def handle_option(option):
    if option == "1": # Débutant (Première position)
        jeu(['1'])
        
    elif option == "2": # Intermédiaire (positions 1,2,3,4)
        jeu(['1','2','3','4'])
        
    elif option == "3": # Avancé (positions 1,2,3,4,5,6,7)
        jeu(['1','2','3','4','5','6','7'])

    elif option == "4": # Personnalisé
        
        print("Choisissez les positions que vous voulez (entre 1 et 7)  / 0 pour terminer")

        option_4 = None
        tab_option_4 = []

        while option_4 != '0' or tab_option_4 == []:

            option_4 = input("Choix: ")

            if option_4 in ['1','2','3','4','5','6','7']:
            
                tab_option_4.append(option_4)
            
            elif option_4 not in ['0','1','2','3','4','5','6','7']:
            
                print("Non ! entre 1 et 7 et 0 pour terminer")
        
        print("Merci ! Le jeu va commencer")
        
        jeu(tab_option_4)
        
    elif option == "5": # Exit
        print("Au revoir.")
        quit()
        
    else:
        print("T'es con ! Entre un nombre entre 1 et 5 !")

def generate_question(tab_position):
    strings = ['Mi', 'La', 'Ré', 'Sol']
    fingers = ['premier', 'second', 'troixième', 'quatrième']
    
    random_string = random.choice(strings)
    random_position = random.choice(tab_position)
    random_finger = random.choice(fingers)
    
    return random_string, random_position, random_finger

def get_note(string, position, finger):
    string_notes = {
        'Mi': { 
            '1' : ['Fa#','Sol','La','Si'], 
            '2' : ['Sol','La','So','Do'],
            '3' : ['La','Si','Do','Ré'],
            '4' : ['Si','Do','Ré','Mi'],
            '5' : ['Do','Ré','Mi','Fa#'],
            '6' : ['Do','Ré','Ré#','Fa#'],
            '7' : ['Do','Ré','Ré#','Fa#']
            },
        'La': { 
            '1' : ['Si','Do','Ré','Mi'], 
            '2' : ['Do','Ré','Mi','Fa#'],
            '3' : ['Ré','Mi','Fa#','Sol'],
            '4' : ['Mi','Fa#','Sol','La'],
            '5' : ['Fa#','Sol','La','Si'],
            '6' : ['Sol','La','Si','Do'],
            '7' : ['La','Si','Do','Ré']
            },
        'Ré': {
            '1' : ['Mi','Fa#','Sol','La'],
            '2' : ['Fa#','Sol','La','Si'],
            '3' : ['Sol','La','Si','Do'],
            '4' : ['La','Si','Do','Ré'],
            '5' : ['Si','Do','Ré','Mi'],
            '6' : ['Do','Ré','Mi','Fa#'],
            '7' : ['Ré','Mi','Fa#','Sol']
            },
        'Sol': { 
            '1' : ['La','Si','Do','Ré'], 
            '2' : ['Si','Do','Ré','Mi'],
            '3' : ['Do','Ré','Mi','Fa#'],
            '4' : ['Ré','Mi','Fa#','Sol'],
            '5' : ['Mi','Fa#','Sol','La'],
            '6' : ['Fa#','Sol','La','Si'],
            '7' : ['Sol','La','Si','Do']
            },
    }
    
    finger_index = {'premier': 0, 'second': 1, 'troixième': 2, 'quatrième': 3}[finger]
    
    note = string_notes[string][position][finger_index]
    
    return note 

def check_answer(user_input, note):
    user_input = user_input.capitalize()
    note = note.capitalize()
    
    equivalents = {
        'Do#': 'Réb', 'Réb': 'Do#',
        'Ré#': 'Mib', 'Mib': 'Ré#',
        'Fa#': 'Solb', 'Solb': 'Fa#',
        'Sol#': 'Lab', 'Lab': 'Sol#',
        'La#': 'Sib', 'Sib': 'La#',
    }
    
    if user_input == note:
        return True
    
    if note in equivalents and user_input == equivalents[note]:
        return True
    
    return False

def jeu(tab_position):

    correct_answers = 0


    num_questions = input("Entrez le nombre de question: ")
            
    try:
        num_questions = int(num_questions)
    except ValueError:
        print("Fallait entrer un nombre... bon je prends la valeur associée dans la table ASCII")
        
        num_questions = sum(ord(character) for character in num_questions)

        print("Ce qui fait: ",num_questions)

    for _ in range(num_questions):
        string, position, finger = generate_question(tab_position)

        note = get_note(string, position, finger)
        
        positions_mapping = {
            '1': 'première',
            '2': 'deuxième',
            '3': 'troisième',
            '4': 'quatrième',
            '5': 'cinquième',
            '6': 'sixième',
            '7': 'septième'
        }

        user_input = input(f"Quelle note correspond au {finger} doigt en {positions_mapping[position]} position sur la corde de {string} ? ")
        
        if check_answer(user_input, note):
            print("Correct!")
            correct_answers += 1
        else:
            print(f"Incorrect. La bonne réponse est: {note}.")
    
    print(f"Vous avez répondu à {correct_answers} sur {num_questions} questions correctement.")

    quit()

def main():

    print_rules()
    
    while True:

        print_menu()

        option = input("Quelle difficulté voulez-vous ? (1-5): ")      

        handle_option(option)  

        print() #for better visibility


if __name__ == '__main__':
    main()